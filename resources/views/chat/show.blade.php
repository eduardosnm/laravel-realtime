@extends('layouts.app')
@push('styles')
<style type="text/css">
    #users > li {
        cursor: pointer;
    }
</style>
@endpush
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Chat</div>

                    <div class="card-body">
                        <div class="row p-2">
                            <div class="col-10">
                                <div class="row">
                                    <div class="col-12 border rounded-lg p-3">
                                        <ul id="messages" class="list-unstyled overflow-auto" style="height: 45vh">
                                        </ul>
                                    </div>
                                </div>
                                <form action="">
                                    <div class="row py-3">
                                        <div class="col-10">
                                            <input type="text" id="message" class="form-control">
                                        </div>
                                        <div class="col-2">
                                            <button class="btn btn-primary btn-block" id="send">Send</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-2">
                                <p><strong>Online now</strong></p>
                                <ul id="users" class="list-unstyled overflow-auto text-info" style="height: 45vh">
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        const userElement = $("#users");
        const messagesElement = $("#messages");

        Echo.join('chat') // join se une a un canal de presencia
            .here((users) => {  // here recibe la lista de usuarios. Se ejecuta en cuanto el usuario se une al canal
                users.forEach((user, index) => {
                    let element = $('<li></li>');

                    element.attr({"id": user.id, "onClick": "greetUser('"+user.id+"')"});
                    element.text(user.name);

                    userElement.append(element);
                })
            })
            .joining((user) => {   // joining permite reaccionar cuando un usuario nuevo se une al canal
                let element = $('<li></li>');

                element.attr({"id": user.id, "onClick": "greetUser('"+user.id+"')"});
                element.text(user.name);

                userElement.append(element);
            })
            .leaving((user) => {    // leaving se ejecuta cuando un usuario abandona el canal
                $("#"+user.id).closest("li").remove();
            })
            .listen('MessageSend', (e) => {
                let element = $('<li></li>');

                element.attr('id', e.user.id);
                element.text(e.user.name + ': ' + e.message);

                messagesElement.append(element);
            })

        const sendElement = $("#send");
        const messageElement = $("#message");

        sendElement.click(function (e) {
            e.preventDefault();
            window.axios.post('/chat/message', {
                message: messageElement.val()
            });

            messageElement.val(null);
        });

        function greetUser(id) {
            window.axios.post('/chat/greet/'+id);
        }

        Echo.private('chat.greet.{{ auth()->user()->id }}')
            .listen('GreetingSent', (e) => {
                let element = $('<li></li>');

                element.text(e.message).addClass('text-success');

                messagesElement.append(element);
            })

    </script>
@endpush
