@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Users</div>

                    <div class="card-body">
                        <ul id="users">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        const userElement = $("#users");

        window.axios.get('/api/users')
            .then((response) => {
                let users = response.data;

                users.forEach((user, index) => {
                    let element = $('<li></li>');

                    element.attr('id', user.id);
                    element.text(user.name);

                    userElement.append(element);
                })
            });

        Echo.channel('users')
            .listen('UserCreated', (e) => {
                let element = $('<li></li>');

                element.attr('id', e.user.id);
                element.text(e.user.name);

                userElement.append(element);
            })
            .listen('UserUpdated', (e) => {
                $("#"+e.user.id).text(e.user.name);
            })
            .listen('UserDeleted', (e) => {
                $("#"+e.user.id).closest("li").remove();
            })
    </script>

@endpush
