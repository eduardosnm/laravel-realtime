@extends('layouts.app')
@push('styles')
<style type="text/css">
    @keyframes rotate {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(360deg);
        }
    }

    .refresh {
        animation: rotate 1.5s linear infinite;
    }
</style>
@endpush
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Game</div>

                    <div class="card-body">
                        <div class="text-center">
                            <img id="circle" class="" width="250" height="250" src="{{ asset('img/circle.png') }}" alt="">

                            <p id="winner" class="display-1 d-none text-primary">10</p>
                        </div>
                        <hr>

                        <div class="text-center">
                            <label for="" class="fotn-wight-bold h5">Your bet</label>
                            <select name="" id="bet" class="custom-select col-auto">
                                <option selected>Not in</option>

                                @foreach(range(1,12) as $number)
                                    <option value="">{{ $number }}</option>
                                @endforeach
                            </select>
                            <hr>

                            <p class="fotn-wight-bold h5">Remainint time</p>
                            <p id="timer" class="h5 text-danger">Waiting to start</p>
                            <hr>
                            <p id="result" class="h1"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        const circleElement = $("#circle");
        const timerElement = $("#timer");
        const winnerElement = $("#winner");
        const betElement = $("#bet");
        const resultElement = $("#result");

        Echo.channel('game')
            .listen('RemainingTimeChanged', (e) => {
                timerElement.text(e.time);

                circleElement.addClass('refresh');

                winnerElement.addClass('d-none');

                resultElement.text(null);

                circleElement.removeClass('text-success text-danger');
            })
            .listen('WinnerNumberGenerated', (e) => {
                circleElement.removeClass('refresh');

                let winner = e.number;
                winnerElement.text(winner).removeClass('d-none');

                let bet = betElement.find('option:selected').text();

                if(bet == winner){
                    resultElement.text("YOU WIN").addClass('text-success');
                }else {
                    resultElement.text("YOU LOSE").addClass('text-dangers');
                }
            });
    </script>
@endpush
