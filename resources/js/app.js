require('./bootstrap');

Echo.private('notifications')
    .listen('UserSessionChanged', (e) => {
        const notificationElement = $("#notification");

        notificationElement.text(e.message);

        notificationElement.removeClass("invisible");
        notificationElement.removeClass("alert-succes");
        notificationElement.removeClass("alert-danger");

        notificationElement.addClass("alert-"+e.type);
    });
